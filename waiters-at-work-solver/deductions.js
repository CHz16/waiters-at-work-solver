// deductions.js
// 2D grid data structures and logic.


// This deduction is manually made at the very start of any solve and isn't included in the list of cell-by-cell deductions
function deduceInitialSetup(state) {
    // We'll put all the boundary objects at the end of the stack so they're processed first; this is just better for display purposes
    let boundaryPlacements = [], otherPlacements = [];
    for (let row = 0; row < state.grid.length; row++) {
        for (let col = 0; col < state.grid[0].length; col++) {
            if (state.grid[row][col].constructor.name != "Nothing") {
                let arr = (state.grid[row][col].constructor.name == "Boundary" ? boundaryPlacements : otherPlacements);
                arr.push({ thing: state.grid[row][col], location: new Location(row, col) });
            }
        }
    }
    return { message: "Mark initial vision", placements: otherPlacements.concat(boundaryPlacements) };
}

function deduceDinerFacingFood(state, location) {
    if (state.thingAt(location).constructor.name != "Nothing") {
        return;
    }

    let visionStats = state.visionStatsAt(location);
    for (let direction of Direction.allDirections) {
        if (visionStats[direction.rawValue] == Vision.looking) {
            let looker = location.move(direction);
            if (state.thingAt(looker).constructor.name == "Diner") {
                return { message: location.string() + ": Diner must face food", placements: [{thing: new Food(), location: location}], highlight: [location], subhighlight: [looker] };
            }
        };
    }
}

function deduceFoodFromMultipleVisions(state, location) {
    if (state.thingAt(location).constructor.name != "Nothing") {
        return;
    }

    let visionStats = state.visionStatsAt(location);
    let lookingCount = 0;
    for (let direction of Direction.allDirections) {
        lookingCount += (visionStats[direction.rawValue] == Vision.looking ? 1 : 0);
    }
    if (lookingCount > 1) {
        return { message: location.string() + ": Cell seen from multiple directions must be food", placements: [{thing: new Food(), location: location}], highlight: [location] };
    }
}

// This is a less general version of deduceDinerFromLastPossibleCell and could be removed, but it's an easier deduction than the more general case and I like having a message for it individually
function deduceDinerFromDeadEndVision(state, location) {
    let visionStats = state.visionStatsAt(location);
    for (let direction of Direction.allDirections) {
        if (visionStats[direction.rawValue] == Vision.looking && visionStats[direction.reversed.rawValue] == Vision.blocked) {
            return { message: location.string() + ": Dead-ended vision line must end in diner", placements: [{thing: new Diner(direction), location: location}], highlight: [location], subhighlight: [location.move(direction), location.move(direction.reversed)] };
        }
    }
}

function deduceDinerFromDeadEndCell(state, location) {
    let visionStats = state.visionStatsAt(location);
    let openDirection = undefined, blockedDirections = [];
    for (let direction of Direction.allDirections) {
        if (visionStats[direction.rawValue] == Vision.blocked) {
            blockedDirections.push(direction);
        } else if (openDirection !== undefined) {
            return;
        } else {
            openDirection = direction;
        }
    }

    if (blockedDirections.length == 3) {
        return { message: location.string() + ": Dead-end cell must be diner", placements: [{thing: new Diner(openDirection), location: location}], highlight: [location], subhighlight: blockedDirections.map(direction => location.move(direction)) };
    }
}

function deduceFoodInConstrainedCell(state, location) {
    if (state.thingAt(location).constructor.name != "Nothing") {
        return;
    }

    let distances = Direction.allDirections.map(direction => state.visibleInDirectionFrom(direction, location));
    if (Math.max(...distances) < 2) {
        return { message: location.string() + ": Cell with < 2 free cells in all directions must be food", placements: [{thing: new Food(), location: location}], highlight: [location], subhighlight: [] };
    }
}

function deduceVisionAcrossPinnedFood(state, location) {
    if (state.thingAt(location).constructor.name != "Food") {
        return;
    }

    let visionStats = state.visionStatsAt(location);
    let dirs;
    if (visionStats[Direction.left.rawValue] == Vision.blocked) {
        dirs = [Direction.up, Direction.down];
    } else if (visionStats[Direction.up.rawValue] == Vision.blocked) {
        dirs = [Direction.left, Direction.right];
    }

    if (dirs !== undefined) {
        return { message: location.string() + ": Food must have a vision line across it", placements: [{vision: Vision.looking, direction: dirs[0], location: location, reverse: true}, {vision: Vision.looking, direction: dirs[1], location: location, reverse: true}], highlight: [location], subhighlight: [location.move(dirs[0]), location.move(dirs[1])] }
    }
}

function deduceDinerFromConstrainedCorner(state, location) {
    if (state.thingAt(location).constructor.name != "Nothing") {
        return;
    }

    let distances = {};
    for (let direction of Direction.allDirections) {
        distances[direction.rawValue] = state.visibleInDirectionFrom(direction, location);
    }

    for (let direction of Direction.allDirections) {
        if (distances[direction.rawValue] > 1 && distances[direction.reversed.rawValue] == 0 && ((distances[direction.rotated90.rawValue] == 0 && distances[direction.rotated270.rawValue] < 2) || (distances[direction.rotated270.rawValue] == 0 && distances[direction.rotated90.rawValue] < 2))) {
            return { message: location.string() + ": Cell in corner with only one free direction must be diner", placements: [{thing: new Diner(direction), location: location}], highlight: [location], subhighlight: [location.move(direction), location.move(direction).move(direction)] }
        }
    }
}

function deduceCellCantHaveLorTVision(state, location) {
    if (state.thingAt(location).constructor.name != "Nothing") {
        return;
    }

    let visionStats = state.visionStatsAt(location);
    for (let direction of Direction.allDirections) {
        if (visionStats[direction.rawValue] == Vision.looking) {
            let blockedDirection;
            if (visionStats[direction.rotated90.rawValue] == Vision.blocked && visionStats[direction.rotated270.rawValue] == Vision.undecided) {
                blockedDirection = direction.rotated90;
            } else if (visionStats[direction.rotated270.rawValue] == Vision.blocked && visionStats[direction.rotated90.rawValue] == Vision.undecided) {
                blockedDirection = direction.rotated270;
            }
            if (blockedDirection !== undefined) {
                return { message: location.string() + ": Cell with vision line and adjacent X can't see across from X", placements: [{vision: Vision.blocked, direction: blockedDirection.reversed, location: location, reverse: true}], highlight: [location], subhighlight: [location.move(blockedDirection)], contradictions: [location.move(blockedDirection.reversed)] }
            }
        }
    }
}

let allDeducers = [deduceDinerFacingFood, deduceDinerFromDeadEndCell, deduceDinerFromDeadEndVision, deduceFoodFromMultipleVisions, deduceFoodInConstrainedCell, deduceVisionAcrossPinnedFood, deduceDinerFromConstrainedCorner, deduceCellCantHaveLorTVision];
