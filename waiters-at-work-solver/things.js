﻿// things.js
// Entity objects for grid display and game logic.


class Thing {
    updateCell(cell) {
        cell.className = "";
        cell.textContent = "";
    }

    is(thing2) {
        return (this.constructor.name === thing2.constructor.name);
    }

    visionStats() {
        return new VisionStats();
    }
}

class Nothing extends Thing {

}

class Boundary extends Thing {
    updateCell(cell) {
        super.updateCell(cell);
        cell.textContent = "!!!";
    }

    visionStats() {
        let visionStats = new VisionStats();
        visionStats[Direction.left.rawValue] = Vision.blocked;
        visionStats[Direction.right.rawValue] = Vision.blocked;
        visionStats[Direction.up.rawValue] = Vision.blocked;
        visionStats[Direction.down.rawValue] = Vision.blocked;
        return visionStats;
    }
}

class Food extends Thing {
    updateCell(cell) {
        super.updateCell(cell);
        cell.textContent = "🥘";
    }
}

class Plant extends Thing {
    updateCell(cell) {
        super.updateCell(cell);
        cell.textContent = "🌳";
    }

    visionStats() {
        let visionStats = new VisionStats();
        visionStats[Direction.left.rawValue] = Vision.blocked;
        visionStats[Direction.right.rawValue] = Vision.blocked;
        visionStats[Direction.up.rawValue] = Vision.blocked;
        visionStats[Direction.down.rawValue] = Vision.blocked;
        return visionStats;
    }
}

class Diner extends Thing {
    constructor(direction) {
        super();
        this.direction = direction;
    }

    updateCell(cell) {
        super.updateCell(cell);
        cell.textContent = "👩‍🦰";
        if (this.direction == Direction.left) {
            cell.className = "rotate90";
        } else if (this.direction == Direction.up) {
            cell.className = "rotate180";
        } else if (this.direction == Direction.right) {
            cell.className = "rotate270";
        }
    }

    is(thing2) {
        if (!super.is(thing2)) {
            return false;
        }
        return (this.direction === thing2.direction);
    }

    visionStats() {
        let visionStats = new VisionStats();
        visionStats[Direction.left.rawValue] = Vision.blocked;
        visionStats[Direction.right.rawValue] = Vision.blocked;
        visionStats[Direction.up.rawValue] = Vision.blocked;
        visionStats[Direction.down.rawValue] = Vision.blocked;
        visionStats[this.direction.rawValue] = Vision.looking;
        return visionStats;
    }
}
