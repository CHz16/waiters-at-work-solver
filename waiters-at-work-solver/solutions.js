// solutions.js
// Solution data scraped from <https://layton-fugou-en.g-takumi.com/day_category.php?category_id=3> and used for testing purposes
// Puzzles created by the daily puzzle team: Takemasa Aoki, Fumitaka Abe, Daisuke Arai, Takeshi Iwasaki, Mitsuyuki Okuyama, Mitsuhiro Ogura, Kenji Ono, Nobuki Kashihara, Toshio Karino, Takashi Kawasaki, Yuki Kawabe, Takashi Kiyomi, Sumihiro Kobayashi, Yuichi Saito, Yuma Saito, Nobuyuki Sakamoto, Tomonobu Jiku, Katsuya Suzuki, Satoru Suzuki, Masaru Taguchi, Emiko Takeuchi, Soma Tange, Takayuki Tsushima, Kazuhiro Nakamura, Takahiro Hyakkai, Makoto Furukawa, Hisato Mori, Junki Yamada, Nao Arai, Yoshinao Anpuku, Yuuki Oonuki, Junji Takeuchi, and Toru Mizoguchi.


let solutions = [
    // 1
    [">%<vv",
     "#vv%%",
     "v%%^^",
     "%^^##",
     "^>%%<"],

    // 2
    ["v>%%<",
     "%#>%<",
     "^>%<v",
     ">%<#%",
     ">%%<^"],

    // 3
    ["v##v#",
     "%v>%<",
     "^%>%<",
     ">%<^#",
     "#^>%<"],

    // 4
    ["#v>%<",
     ">%<vv",
     ">%<%%",
     ">%<^^",
     "#^>%<"],

    // 5
    ["#v>%<",
     ">%%<v",
     "v%vv%",
     "%^%%%",
     "^#^^^"],

    // 6
    [">%%<v",
     ">%<v%",
     "#vv%^",
     ">%%%<",
     "#^^^#"],

    // 7
    ["#v>%<v#",
     ">%<#>%<",
     ">%%<v^v",
     "#%>%%<%",
     ">%%<^v^",
     "#^#>%%<",
     ">%%<#^#"],

    // 8
    ["#>%%%<v",
     ">%<>%<%",
     ">%<>%<%",
     "v>%%%<%",
     "%>%<vv^",
     "%>%%%%<",
     "^>%<^^#"],

    // 9
    ["v>%%<v#",
     "%>%<>%<",
     "%v#>%%<",
     "^%>%<%v",
     ">%%<#%%",
     ">%%%<^%",
     "#^>%%<^"],

    // 10
    ["#vv>%<v",
     ">%%<#v%",
     "v%^v#%^",
     "%^>%%%<",
     "%v#^>%<",
     "%%#>%%<",
     "^^>%<^#"],

    // 11
    ["#vv>%<#",
     ">%%<vvv",
     ">%%<%%%",
     "v^^v^%^",
     "%#v%>%<",
     "%>%%<^#",
     "^#^^>%<"],

    // 12
    ["#v#v>%<",
     ">%%%<vv",
     ">%<%#%%",
     ">%<^v%^",
     "v^>%%%<",
     "%#>%%%<",
     "^>%<^^#"],

    // 13
    ["v>%<vv#",
     "%#v>%%<",
     "%>%%%%<",
     "^v^>%%<",
     ">%%<%^v",
     ">%%<^#%",
     "#^>%%<^"],

    // 14
    ["vvv>%%<",
     "%%%v>%<",
     "%^^%v#v",
     "^>%%%<%",
     ">%%%%<%",
     ">%<^^#^",
     "#>%%%<#"],

    // 15
    ["#v>%%%<v#",
     ">%<#v>%%<",
     ">%%<%>%%<",
     ">%<v%>%%<",
     "v%>%%<#^#",
     "%^#^%>%%<",
     "^>%<^>%%<"],

    // 16
    ["#vvv>%%%<",
     ">%%%<#v#v",
     "v%^^>%%<%",
     "%^v#>%%<%",
     "%>%<#>%<%",
     "%>%%<#^#^",
     "^#^>%%%<#"],

    // 17
    ["v>%%<>%%<",
     "%#>%%<v#v",
     "^vv>%<%v%",
     ">%%<#v^%%",
     "v^%>%%<^%",
     "%#^>%%<#^",
     "^>%%<^>%<"],

     // The puzzle data from this point on I haven't scraped personally from my Switch copy, but rather took from this site for the original mobile or 3DS version: <https://layton-fugou-en.g-takumi.com/day_category.php?category_id=3>. I don't really expect there to be, but theoretically there may be puzzle changes in the Switch version in the future puzzles that haven't been unlocked yet.

     // 18
    ["#v>%<#vvv",
     ">%<>%<%%%",
     ">%%<vv^^%",
     "v%>%%%%<^",
     "%^>%%%%%<",
     "^>%<%^>%<",
     ">%<#^>%<#"],

     // 19
    ["#>%%%<vv#",
     ">%<v>%%%<",
     "v>%%%<%^v",
     "%vv^>%%<%",
     "^%%>%<%v%",
     ">%%%%<^%^",
     "#^^>%%<^#"],

     // 20
    ["#>%<v>%<#",
     "#vv#%>%%<",
     "v%%#^#>%<",
     "%^%>%<vv#",
     "%>%<#>%%<",
     "%>%%<#^^#",
     "^#^>%%%<#"],

     // 21
    ["v>%%%<vvv",
     "%>%<v#%%%",
     "^vv>%<%%%",
     ">%%<%v^^%",
     "v%%>%%%<^",
     "%^^#^%>%<",
     "^>%%<^>%<"]
];
