// state.js
// Encapsulation of a solve state for a puzzle.


let Vision = { undecided: 0, looking: 1, blocked: 2 };
class VisionStats {
    constructor() {
        this[Direction.left.rawValue] = Vision.undecided;
        this[Direction.right.rawValue] = Vision.undecided;
        this[Direction.up.rawValue] = Vision.undecided;
        this[Direction.down.rawValue] = Vision.undecided;
    }
}

class State {
    constructor(grid) {
        this.grid = State.deepCopy(grid);

        this.vision = [];
        for (let row = 0; row < this.grid.length; row++) {
            let visionRow = [];
            for (let col = 0; col < this.grid[0].length; col++) {
                visionRow.push(new VisionStats());
            }
            this.vision.push(visionRow);
        }
    }

    clone() {
        return State.deepCopy(this);
    }

    thingAt(location) {
        return this.grid[location.row][location.col];
    }

    visionStatsAt(location) {
        return this.vision[location.row][location.col];
    }

    visibleInDirectionFrom(direction, location) {
        let count = 0, currentLocation = location;
        while (this.visionStatsAt(currentLocation)[direction.rawValue] != Vision.blocked) {
            count += 1;
            currentLocation = currentLocation.move(direction);
        }
        return count;
    }

    place(placement) {
        let newState = this.clone();
        let placements = [], subhighlight = [];

        if (placement.vision !== undefined) {
            if (newState.isInBounds(placement.location)) {
                let visionStats = newState.vision[placement.location.row][placement.location.col];
                if (visionStats[placement.direction.rawValue] != placement.vision) {
                    if (visionStats[placement.direction.rawValue] != Vision.undecided) {
                        let location = (placement.vision == Vision.looking ? placement.location : placement.location.move(placement.direction));
                        let target = (placement.vision == Vision.looking ? placement.location.move(placement.direction) : placement.location);
                        return { state: newState, contradictions: [location], subhighlight: [target], message: "Contradiction: " + this.thingAt(location).constructor.name + "'s vision is blocked" };
                    } else {
                        visionStats[placement.direction.rawValue] = placement.vision;
                        if (placement.reverse) {
                            placements.push({ vision: placement.vision, location: placement.location.move(placement.direction), direction: placement.direction.reversed });
                        }
                        if (this.thingAt(placement.location).constructor.name == "Food") {
                            placements.push({ vision: placement.vision, location: placement.location, direction: placement.direction.reversed, reverse: true });
                        }
                    }
                }
            }
        } else {
            if (newState.thingAt(placement.location).constructor.name != "Nothing" && !newState.thingAt(placement.location).is(placement.thing)) {
                return { state: newState, contradictions: [placement.location], subhighlight: [], message: "Contradiction: tried to place " + placement.thing.constructor.name + " where there's already a " + newState.thingAt(placement.location).constructor.name };
            }
            newState.grid[placement.location.row][placement.location.col] = placement.thing;

            let visionStats = placement.thing.visionStats();
            for (let direction of Direction.allDirections) {
                if (visionStats[direction.rawValue] != Vision.undecided) {
                    placements.push({ vision: visionStats[direction.rawValue], location: placement.location, direction: direction, reverse: true });
                }

                let currentVision = this.vision[placement.location.row][placement.location.col][direction.rawValue];
                if (currentVision != Vision.undecided && placement.thing.constructor.name == "Food") {
                    placements.push({ vision: currentVision, location: placement.location, direction: direction.reversed, reverse: true });
                }
            }
        }

        return { state: newState, placements: placements, highlight: [], subhighlight: [] };
    }

    isInBounds(location) {
        return (location.row >= 0 && location.row < this.grid.length && location.col >= 0 && location.col < this.grid[0].length);
    }

    static deepCopy(o) {
        if (typeof o != "object") {
            return o;
        } else if (o.constructor.name == "State") {
            let newState = new State(o.grid);
            newState.vision = State.deepCopy(o.vision);
            return newState;
        } else if (o.constructor.name == "Array") {
            let newArray = [];
            for (let member of o) {
                newArray.push(State.deepCopy(member));
            }
            return newArray;
        } else if (o instanceof Thing) {
            return o;
        } else if (o.constructor.name == "VisionStats") {
            let newVisionStats = new VisionStats();
            newVisionStats[Direction.left.rawValue] = o[Direction.left.rawValue];
            newVisionStats[Direction.right.rawValue] = o[Direction.right.rawValue];
            newVisionStats[Direction.up.rawValue] = o[Direction.up.rawValue];
            newVisionStats[Direction.down.rawValue] = o[Direction.down.rawValue];
            return newVisionStats;
        } else {
            console.warn("hmmmmmm you're cloning something unrecognized, chief", o);
        }
    }
}
