// gridlogic.js
// 2D grid data structures and logic.


let Direction = {
    left: { rawValue: 0, xDelta: -1, yDelta: 0 },
    right: { rawValue: 1, xDelta: 1, yDelta: 0 },
    up: { rawValue: 2, xDelta: 0, yDelta: -1 },
    down: { rawValue: 3, xDelta: 0, yDelta: 1 }
};
Direction.left.rotated90 = Direction.up;
Direction.left.reversed = Direction.right;
Direction.left.rotated270 = Direction.down;
Direction.right.rotated90 = Direction.down;
Direction.right.reversed = Direction.left;
Direction.right.rotated270 = Direction.up;
Direction.up.rotated90 = Direction.right;
Direction.up.reversed = Direction.down;
Direction.up.rotated270 = Direction.left;
Direction.down.rotated90 = Direction.left;
Direction.down.reversed = Direction.up;
Direction.down.rotated270 = Direction.right;
Direction.allDirections = [Direction.left, Direction.right, Direction.up, Direction.down];

class Location {
    constructor(row, col) {
        this.row = row;
        this.col = col;
    }

    move(direction) {
        return new Location(this.row + direction.yDelta, this.col + direction.xDelta);
    }

    string() {
        return "R" + this.row + "C" + this.col;
    }
}
