// main.js
// Solver and UI setup.


//
// LAYOUT PARAMETERS
//

const containerWidth = 960, containerHeight = 540;
const tableMaxWidth = 720, tableMaxHeight = 540;
const cellMaxSize = 64;


//
// GLOBALS
//

let lastUpdateTime;
let inputGrid;

let fontSizeForCellSize = []; // calculated in init

let solveStepList = [], currentSolveStep = undefined;
let nextSolveStepTimeout = undefined;
let unfinishedLocations = [];

const debugMode = (typeof solutions !== "undefined");
let testingPuzzle;


//
// INITIALIZATION
//

window.addEventListener("load", init);
function init() {
    // Precalculate the biggest font size we can fit into every cell size
    document.styleSheets[0].insertRule("#grid td { font-size: 0px; }", 0);
    let testCell = document.getElementById("grid").children[0].children[0];
    let fontSize = 0;
    while (fontSizeForCellSize[cellMaxSize] === undefined) {
        let currentSize = Math.max(testCell.clientWidth, testCell.clientHeight);
        document.styleSheets[0].deleteRule(0);
        document.styleSheets[0].insertRule("#grid td { font-size: " + (fontSize + 1) + "px; }", 0);
        let nextSize = Math.max(testCell.clientWidth, testCell.clientHeight);

        for (let s = currentSize; s < nextSize; s++) {
            fontSizeForCellSize[s] = fontSize;
        }
        fontSize += 1;
    }
    document.getElementById("grid").removeChild(document.getElementById("grid").firstChild);

    // Populate the puzzle selector and set it up to change puzzles
    for (let i = 0; i < puzzles.length; i++) {
        let option = document.createElement("option");
        option.value = i;
        option.text = "Puzzle " + (i + 1);
        if (i == 0) {
            option.selected = "selected";
        }
        document.getElementById("puzzleselect").add(option);
    }
    document.getElementById("puzzleselect").addEventListener("input", function(event) {
        clearSolve();
        if (event.target.selectedIndex == 0) {
            for (let row = 1; row < inputGrid.length - 1; row++) {
                for (let col = 1; col < inputGrid[0].length - 1; col++) {
                    inputGrid[row][col] = new Nothing();
                }
            }
            clearSolve();
            updateControls();
            refreshGrid();
        } else {
            loadPuzzle(event.target.selectedIndex - 1);
        }
    });

    // Set up the resize button
    document.getElementById("resizebutton").addEventListener("click", function(event) {
        // Read & parse new dimensions
        let result = prompt("Enter the new dimensions, width first (max 20) and then height (max 15):", (inputGrid[0].length - 2) + "x" + (inputGrid.length - 2));
        if (result === null) {
            return;
        }
        let components = result.split(/x/i, 2);
        if (components.length < 2) {
            return;
        }
        let width = parseInt(components[0], 10);
        if (isNaN(width) || width < 3 || width > 20) {
            return;
        }
        let height = parseInt(components[1], 10);
        if (isNaN(height) || height < 3 || height > 15) {
            return;
        }
        clearSolve();

        // If we're expanding the grid, clear the boundary objects from the bottom and/or right
        if (height + 2 > inputGrid.length) {
            for (let col = 1; col < inputGrid[0].length; col++) {
                inputGrid[inputGrid.length - 1][col] = new Nothing();
            }
        }
        if (width + 2 > inputGrid[0].length) {
            for (let row = 1; row < inputGrid.length - 1; row++) {
                inputGrid[row][inputGrid[0].length - 1] = new Nothing();
            }
        }

        // Match the # of rows and cols
        if (inputGrid.length > height + 2) {
            inputGrid = inputGrid.slice(0, height + 2);
        }
        while (inputGrid.length < height + 2) {
            inputGrid.push([]);
        }
        for (let row = 0; row < height + 2; row++) {
            if (inputGrid[row].length > width + 2) {
                inputGrid[row] = inputGrid[row].slice(0, width + 2);
            }
            while (inputGrid[row].length < width + 2) {
                inputGrid[row].push(new Nothing());
            }
        }

        // Set the new boundary objects
        for (let col = 0; col < width + 2; col++) {
            inputGrid[0][col] = new Boundary();
            inputGrid[height + 1][col] = new Boundary();
        }
        for (let row = 1; row < height + 1; row++) {
            inputGrid[row][0] = new Boundary();
            inputGrid[row][width + 1] = new Boundary();
        }

        clearSolve();
        updateControls();
        refreshGrid();
        document.getElementById("puzzleselect").selectedIndex = -1; // after editing, clear the field so the user can re-select the current option to reload
    });

    // Set up the table to receive clicks
    document.getElementById("grid").addEventListener("click", function(event) {
        cycleCell(event.target, 1);
    });
    document.getElementById("grid").addEventListener("contextmenu", function(event) {
        cycleCell(event.target, -1);
        event.preventDefault();
    });

    // Set up the table vision overlays to intercept clicks
    let nope = function(event) { alert("You can't modify the grid while a solve is in progress. Hit the Reset button if you want to make changes."); event.preventDefault(); };
    document.getElementById("visionvertical").addEventListener("click", nope);
    document.getElementById("visionvertical").addEventListener("contextmenu", nope);
    document.getElementById("visionhorizontal").addEventListener("click", nope);
    document.getElementById("visionhorizontal").addEventListener("contextmenu", nope);

    // Set up the deduction list to receive clicks
    document.getElementById("deductionlist").addEventListener("click", function(event) {
        if (event.target.tagName != "LI") {
            return;
        }
        clearNextSolveStepTimeout();
        takeSolveStep(parseInt(event.target.dataset.index, 10));
    });

    // Set up the solve controls
    document.getElementById("startstopbutton").addEventListener("click", function(event) {
        if (nextSolveStepTimeout === undefined) {
            setNextSolveStepTimeout();
            takeSolveStep(currentSolveStep + 1);
        } else {
            clearNextSolveStepTimeout();
        }
        updateControls();
    });
    document.getElementById("stepbackwardbutton").addEventListener("click", function(event) {
        clearNextSolveStepTimeout();
        takeSolveStep(currentSolveStep - 1);
        updateControls();
    });
    document.getElementById("stepforwardbutton").addEventListener("click", function(event) {
        clearNextSolveStepTimeout();
        takeSolveStep(currentSolveStep + 1);
        updateControls();
    });
    document.getElementById("resetbutton").addEventListener("click", function(event) {
        clearSolve();
        updateControls();
        refreshGrid();
    });

    document.getElementById("speedselect").addEventListener("input", function(event) {
        if (nextSolveStepTimeout !== undefined) {
            clearNextSolveStepTimeout();
            setNextSolveStepTimeout();
        }
    });

    // If solutions.js is included as a source, then show and enable the test suite button
    if (debugMode) {
        document.getElementById("testbutton").style.display = "block";
        document.getElementById("testbutton").addEventListener("click", function(event) {
            document.getElementById("resetbutton").click();
            document.getElementById("speedselect").selectedIndex = 3;
            testingPuzzle = 1;
            testPuzzle();
        });
    }

    // Start by loading the first puzzle
    loadPuzzle(0);
}


//
// SOLVE STEP MANAGEMENT
//

function takeSolveStep(i) {
    if (currentSolveStep !== undefined) {
        document.getElementById("deductionlist").children[currentSolveStep].className = "";
    }

    if (solveStepList.length == 0) {
        currentSolveStep = 0;
        addSolveStep({state: new State(inputGrid), message: "Start"});
        unfinishedLocations = [];
        for (let row = 1; row < inputGrid.length - 1; row++) {
            for (let col = 1; col < inputGrid[0].length - 1; col++) {
                unfinishedLocations.push(new Location(row, col));
            }
        }
    } else {
        currentSolveStep = i;
        if (currentSolveStep == solveStepList.length) {
            makeDeduction();
        }
    }

    if (debugMode) {
        console.log(currentSolveStep, solveStepList[currentSolveStep]);
    }

    if (nextSolveStepTimeout !== undefined && !solveStepList[currentSolveStep].stop) {
        clearNextSolveStepTimeout();
        setNextSolveStepTimeout();
    } else {
        clearNextSolveStepTimeout();
    }

    // Scroll the selected deduction entry into view if it's not fully visible
    // The adjustments by 2 are there for padding
    let deductionList = document.getElementById("deductionlist"), li = deductionList.children[currentSolveStep];
    li.className = "selected";
    let liTop = li.offsetTop - deductionList.offsetTop, liBottom = liTop + li.offsetHeight;
    if (liTop < deductionList.scrollTop) {
        deductionList.scrollTop = liTop - 2;
    } else if (liBottom > (deductionList.scrollTop + deductionList.offsetHeight)) {
        deductionList.scrollTop = liBottom - deductionList.offsetHeight + 2;
    }

    updateControls();
    refreshGrid();
    if (testingPuzzle !== undefined && nextSolveStepTimeout === undefined) {
        finishTest();
    }
}

function makeDeduction() {
    let deduction;
    if (currentSolveStep == 1) {
        deduction = deduceInitialSetup(solveStepList[0].state);
    } else {
        let state = solveStepList[currentSolveStep - 1].state;

        // Prune all locations where we've placed something and the vision is complete
        let i = 0;
        while (i < unfinishedLocations.length) {
            let undecidedCount = 0;
            for (let direction of Direction.allDirections) {
                undecidedCount += (state.visionStatsAt(unfinishedLocations[i])[direction.rawValue] == Vision.undecided ? 1 : 0);
            }
            if (state.thingAt(unfinishedLocations[i]).constructor.name != "Nothing" && undecidedCount == 0) {
                unfinishedLocations.splice(i, 1);
                continue;
            }
            i++;
        }

        // Look through every unfinished cell and try every deduction on them until we find one
        deductionLoop: {
            for (let deducer of allDeducers) {
                for (let location of unfinishedLocations) {
                    deduction = deducer(state, location);
                    if (deduction !== undefined) {
                        break deductionLoop;
                    }
                }
            }
        }
    }

    if (deduction === undefined) {
        if (unfinishedLocations.length == 0) {
            let cleanedState = solveStepList[currentSolveStep - 1].state.clone();
            cleanedState.vision = undefined;
            addSolveStep({ message: "Finished", state: cleanedState, stop: true });
        } else {
            addSolveStep({ message: "No deductions found", state: solveStepList[currentSolveStep - 1].state, stop: true });
        }
        return;
    }

    let currentState = solveStepList[currentSolveStep - 1].state;
    while (deduction.placements.length > 0) {
        let placement = deduction.placements.pop();
        let placementResult = currentState.place(placement);
        if (placementResult.contradictions !== undefined) {
            addSolveStep({ message: deduction.message + "\n" + placementResult.message, state: currentState, highlight: placementResult.highlight, subhighlight: placementResult.subhighlight, contradictions: placementResult.contradictions, stop: true });
            return;
        } else {
            deduction.placements = deduction.placements.concat(placementResult.placements);
            if (deduction.highlight !== undefined) {
                deduction.highlight = deduction.highlight.concat(placementResult.highlight);
            }
            if (deduction.subhighlight !== undefined) {
                deduction.subhighlight = deduction.subhighlight.concat(placementResult.subhighlight);
            }
            if (deduction.contradictions !== undefined && placementResult.contradictions !== undefined) {
                deduction.contradictions = deduction.contradictions.concat(placementResult.contradictions);
            }
            currentState = placementResult.state;
        }
    }
    addSolveStep({ message: deduction.message, state: currentState, highlight: deduction.highlight, subhighlight: deduction.subhighlight, contradictions: deduction.contradictions });
}

function addSolveStep(solveStep) {
    solveStepList.push(solveStep);
    let li = document.createElement("li");
    li.textContent = solveStep.message;
    li.dataset.index = currentSolveStep;

    let deductionList = document.getElementById("deductionlist");
    deductionList.appendChild(li);
}

function setNextSolveStepTimeout() {
    nextSolveStepTimeout = window.setTimeout(function() { takeSolveStep(currentSolveStep + 1); }, document.getElementById("speedselect").options[document.getElementById("speedselect").selectedIndex].value);
}

function clearNextSolveStepTimeout() {
    window.clearTimeout(nextSolveStepTimeout);
    nextSolveStepTimeout = undefined;
}

function clearSolve() {
    clearNextSolveStepTimeout();
    solveStepList = [];
    currentSolveStep = undefined;

    let deductionList = document.getElementById("deductionlist");
    while (deductionList.firstChild !== null) {
        deductionList.removeChild(deductionList.firstChild);
    }
}


//
// GRID UPDATES
//

function cycleCell(cell, delta) {
    // event.target can be a non-cell if the user clicks and drags
    if (cell.tagName != "TD") {
        return;
    }

    let things = [new Nothing(), new Diner(Direction.down), new Diner(Direction.left), new Diner(Direction.up), new Diner(Direction.right), new Food(), new Plant()];

    let row = cell.dataset.row, col = cell.dataset.col;
    let i = things.findIndex((thing) => thing.is(inputGrid[row][col]));
    inputGrid[row][col] = things[(i + delta + things.length) % things.length];
    inputGrid[row][col].updateCell(cell);

    document.getElementById("puzzleselect").selectedIndex = -1; // after editing, clear the field so the user can re-select the current option to reload
}

function loadPuzzle(i) {
    let puzzle = puzzles[i];
    inputGrid = [];
    for (let row = 0; row < puzzle.length + 2; row++) {
        let gridRow = [];
        for (let col = 0; col < puzzle[0].length + 2; col++) {
            if (row == 0 || row == puzzle.length + 1 || col == 0 || col == puzzle[0].length + 1) {
                gridRow.push(new Boundary());
            } else if (puzzle[row - 1][col - 1] == "#") {
                gridRow.push(new Plant());
            } else if (puzzle[row - 1][col - 1] == "%") {
                gridRow.push(new Food());
            } else if (puzzle[row - 1][col - 1] == "^") {
                gridRow.push(new Diner(Direction.up));
            } else if (puzzle[row - 1][col - 1] == "v") {
                gridRow.push(new Diner(Direction.down));
            } else if (puzzle[row - 1][col - 1] == "<") {
                gridRow.push(new Diner(Direction.left));
            } else if (puzzle[row - 1][col - 1] == ">") {
                gridRow.push(new Diner(Direction.right));
            } else {
                gridRow.push(new Nothing());
            }
        }
        inputGrid.push(gridRow);
    }
    refreshGrid();
    updateControls();
}


//
// UI
//

function updateControls() {
    document.getElementById("stepbackwardbutton").disabled = !(solveStepList.length > 1 && currentSolveStep > 0);
    document.getElementById("stepforwardbutton").disabled = (currentSolveStep !== undefined && solveStepList[currentSolveStep].stop);
    document.getElementById("startstopbutton").disabled = (currentSolveStep !== undefined && solveStepList[currentSolveStep].stop);
    document.getElementById("startstopbutton").textContent = (nextSolveStepTimeout === undefined) ? "Start" : "Stop";
    document.getElementById("resetbutton").disabled = !(solveStepList.length > 0);
}

function resizeTable(table, width, height) {
    while (table.childNodes.length > height) {
        table.removeChild(table.lastChild);
    }
    while (table.childNodes.length < height) {
        table.appendChild(document.createElement("tr"));
    }

    for (let row = 0; row < height; row++) {
        let tableRow = table.childNodes[row];
        while (tableRow.childNodes.length > width) {
            tableRow.removeChild(tableRow.lastChild);
        }
        while (tableRow.childNodes.length < width) {
            let cell = document.createElement("td");
            cell.dataset.row = row + 1;
            cell.dataset.col = tableRow.childNodes.length + 1;
            tableRow.appendChild(cell);
        }
    }
}

function refreshGrid() {
    let grid, vision, highlight, subhighlight, contradictions;
    if (solveStepList.length > 0) {
        grid = solveStepList[currentSolveStep].state.grid;
        vision = solveStepList[currentSolveStep].state.vision;
        highlight = solveStepList[currentSolveStep].highlight;
        subhighlight = solveStepList[currentSolveStep].subhighlight;
        contradictions = solveStepList[currentSolveStep].contradictions;
    } else {
        grid = inputGrid;
    }

    let puzzle = document.getElementById("grid");
    let height = grid.length - 2;
    let width = grid[0].length - 2;
    resizeTable(puzzle, width, height);

    let freeTableWidth = tableMaxWidth - 2 * (width + 1);
    let freeTableHeight = tableMaxHeight - 2 * (height + 1);
    let cellSize = Math.min(cellMaxSize, Math.floor(freeTableWidth / width), Math.floor(freeTableHeight / height));
    document.styleSheets[0].deleteRule(0);
    document.styleSheets[0].insertRule("#gridcontainer td { width: " + cellSize + "px; height: " + cellSize + "px; font-size: " + fontSizeForCellSize[cellSize] + "px; }", 0);

    for (let row = 0; row < height; row++) {
        let puzzleRow = puzzle.childNodes[row];

        for (let col = 0; col < width; col++) {
            let subhighlighted = (puzzleRow.children[col].className.indexOf("subhighlight") != -1);
            let highlighted = (puzzleRow.children[col].className.indexOf("highlight") != -1);
            let contradicted = (puzzleRow.children[col].className.indexOf("contradiction") != -1);
            let odd = (row + col) % 2 == 1;

            grid[row + 1][col + 1].updateCell(puzzleRow.children[col]);
            if (highlight !== undefined && highlight.findIndex((element) => (element.row == row + 1 && element.col == col + 1)) != -1) {
                puzzleRow.children[col].className += " highlight";
            } else if (subhighlight !== undefined && subhighlight.findIndex((element) => (element.row == row + 1 && element.col == col + 1)) != -1) {
                puzzleRow.children[col].className += " subhighlight";
            } else if (contradictions !== undefined && contradictions.findIndex((element) => (element.row == row + 1 && element.col == col + 1)) != -1) {
                puzzleRow.children[col].className += " contradiction";
            } else if (subhighlighted) {
                puzzleRow.children[col].className += " subhighlightto" + (odd ? "odd" : "even");
            } else if (highlighted) {
                puzzleRow.children[col].className += " highlightto" + (odd ? "odd" : "even");
            } else if (contradicted) {
                puzzleRow.children[col].className += " contradictionto" + (odd ? "odd" : "even");
            }
        }
    }

    if (vision !== undefined) {
        let visionVertical = document.getElementById("visionvertical"), visionHorizontal = document.getElementById("visionhorizontal")
        visionVertical.style.display = "table";
        visionVertical.style.top = -Math.floor(cellSize / 2) + "px";
        resizeTable(visionVertical, width, height + 1);
        visionHorizontal.style.display = "table";
        visionHorizontal.style.top = 0;
        visionHorizontal.style.left = -Math.floor(cellSize / 2) + "px";
        resizeTable(visionHorizontal, width + 1, height);

        for (let row = 0; row < height; row++) {
            for (let col = 0; col < width; col++) {
                if (vision[row + 1][col + 1][Direction.left.rawValue] != Vision.undecided) {
                    visionHorizontal.childNodes[row].childNodes[col].textContent = (vision[row + 1][col + 1][Direction.left.rawValue] == Vision.blocked ? "X" : "—");
                } else {
                    visionHorizontal.childNodes[row].childNodes[col].textContent = "";
                }
                if (vision[row + 1][col + 1][Direction.right.rawValue] != Vision.undecided) {
                    visionHorizontal.childNodes[row].childNodes[col + 1].textContent = (vision[row + 1][col + 1][Direction.right.rawValue] == Vision.blocked ? "X" : "—");
                } else {
                    visionHorizontal.childNodes[row].childNodes[col + 1].textContent = "";
                }
                if (vision[row + 1][col + 1][Direction.up.rawValue] != Vision.undecided) {
                    visionVertical.childNodes[row].childNodes[col].textContent = (vision[row + 1][col + 1][Direction.up.rawValue] == Vision.blocked ? "X" : "|");
                } else {
                    visionVertical.childNodes[row].childNodes[col].textContent = "";
                }
                if (vision[row + 1][col + 1][Direction.down.rawValue] != Vision.undecided) {
                    visionVertical.childNodes[row + 1].childNodes[col].textContent = (vision[row + 1][col + 1][Direction.down.rawValue] == Vision.blocked ? "X" : "|");
                } else {
                    visionVertical.childNodes[row + 1].childNodes[col].textContent = "";
                }
            }
        }
    } else {
        document.getElementById("visionvertical").style.display = "none";
        document.getElementById("visionhorizontal").style.display = "none";
    }
}


//
// TESTING
//

function testPuzzle() {
    document.getElementById("puzzleselect").selectedIndex = testingPuzzle;
    document.getElementById("puzzleselect").dispatchEvent(new Event("input"));
    document.getElementById("startstopbutton").click();
}

function finishTest() {
    let solvedGrid = solveStepList[currentSolveStep].state.grid;
    for (let row = 1; row < solvedGrid.length - 1; row++) {
        for (let col = 1; col < solvedGrid[0].length - 1; col++) {
            let code = "!";
            if (solvedGrid[row][col].constructor.name == "Food") {
                code = "%";
            } else if (solvedGrid[row][col].constructor.name == "Plant") {
                code = "#";
            } else if (solvedGrid[row][col].constructor.name == "Diner") {
                if (solvedGrid[row][col].direction == Direction.up) {
                    code = "^";
                } else if (solvedGrid[row][col].direction == Direction.down) {
                    code = "v";
                } else if (solvedGrid[row][col].direction == Direction.left) {
                    code = "<";
                } else if (solvedGrid[row][col].direction == Direction.right) {
                    code = ">";
                }
            }
            if (solutions[testingPuzzle - 1][row - 1][col - 1] != code) {
                alert("Grid doesn't match reference solution!");
                return;
            }
        }
    }

    document.getElementById("resetbutton").click();
    testingPuzzle += 1;
    if (testingPuzzle >= document.getElementById("puzzleselect").options.length) {
        testingPuzzle = undefined;
        alert("All tests passed!");
    } else {
        testPuzzle();
    }
}

function debug(s) {
    document.getElementById("debug").innerHTML += "<br />" + s;
}
