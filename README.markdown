Waiters at Work solver
======================

A deduction-based solver for the Waiters at Work daily puzzle from *Layton's Mystery Journey*, made for [GAMES MADE QUICK??? FOUR+](https://itch.io/jam/games-made-quick-four-plus). Usable here: https://chz.itch.io/waiters-for-work-solver

All the code is by me and released under MIT (see LICENSE.txt). The puzzle data is all from the game; the specific authors of individual puzzles aren't credited, so here's everyone credited for the daily puzzles: Takemasa Aoki, Fumitaka Abe, Daisuke Arai, Takeshi Iwasaki, Mitsuyuki Okuyama, Mitsuhiro Ogura, Kenji Ono, Nobuki Kashihara, Toshio Karino, Takashi Kawasaki, Yuki Kawabe, Takashi Kiyomi, Sumihiro Kobayashi, Yuichi Saito, Yuma Saito, Nobuyuki Sakamoto, Tomonobu Jiku, Katsuya Suzuki, Satoru Suzuki, Masaru Taguchi, Emiko Takeuchi, Soma Tange, Takayuki Tsushima, Kazuhiro Nakamura, Takahiro Hyakkai, Makoto Furukawa, Hisato Mori, Junki Yamada, Nao Arai, Yoshinao Anpuku, Yuuki Oonuki, Junji Takeuchi, and Toru Mizoguchi.
